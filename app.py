from flask import Flask, render_template, url_for, flash, redirect
from flask_login import login_user, LoginManager, login_required, logout_user, current_user
from flask_bcrypt import Bcrypt
from flask_bootstrap import Bootstrap5
from bson.objectid import ObjectId
from my_models import User, Book, Order, create_query, create_fields
from my_forms import RegisterForm, LoginForm, UserSearchForm, EditUserForm, BookForm, BookSearchForm, OrderForm


app = Flask(__name__)
bcrypt = Bcrypt(app)
app.config['SECRET_KEY'] = 'thisisasecretkey'
app.config['BOOTSTRAP_USE_MINIFIED'] = False
Bootstrap5(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.login_message_category = "info"
login_manager.login_message = "Please log in and make sure your account is approved."

@login_manager.user_loader
def load_user(user_id):
    _id = ObjectId(user_id)
    return User.get_user({"_id": _id})

###############################################
#   Orders
###############################################

@app.route('/orders', methods=['GET'])
@login_required
def orders():
    if current_user.admin:
        orders = Order.get_orders({"active": True})
        history = Order.get_orders({"active": False})
    else:
        _id = ObjectId(current_user.id)
        orders = Order.get_orders({"user_id": _id, "active": True})
        history = Order.get_orders({"user_id": _id, "active": False})
    if not orders and not history:
            flash("No order was found.", "info")
    return render_template('orders.html', orders=orders, history=history)

@app.route('/order_book/<book_id>')
@login_required
def order_book(book_id):
    book_id = ObjectId(book_id)
    book = Book.get_book({"_id": book_id})
    if book:
        user_id = ObjectId(current_user.id)
        exist = Order.get_order({"user_id": user_id, "book_id": book_id, "active": True})
        user_book_count = len(Order.get_orders({"user_id": user_id, "active": True}))
        book_user_count = len(Order.get_orders({"book_id": book_id, "active": True}))
        if not current_user.ban and current_user.approved and not exist and user_book_count < 6 and book_user_count < book.copy_count:
            Order((user_id, book_id))
            flash("Order was created.", "info")
        else:
            flash("No permission!", "info")
    else:
        flash('Book does not exist!', 'info')
    return redirect(url_for('home'))

@app.route('/order_add', methods=['GET', 'POST'])
@login_required
def order_add():
    if current_user.admin:
        form = OrderForm()
        if form.validate_on_submit():
            user = User.get_user({"username": form.username.data})
            book = Book.get_book({"name": form.name.data})
            if user and book:
                user_id = ObjectId(user.id)
                book_id = ObjectId(book.id)
                exist = Order.get_order({"user_id": user_id, "book_id": book_id, "active": True})
                user_book_count = len(Order.get_orders({"user_id": user_id, "active": True}))
                book_user_count = len(Order.get_orders({"book_id": book_id, "active": True}))
                if not user.ban and user.approved and not exist and user_book_count < 6 and book_user_count < book.copy_count:
                    Order((user_id, book_id))
                    flash('Order was added!', 'info')
                    return redirect(url_for('orders'))
                else:
                    flash('No permission!', 'info')
            else:
                flash('User or book does not exist!', 'info')
        return render_template('register.html', form=form, text="Add order")
    else:
        flash('No permission!', 'info')
        return redirect(url_for('orders'))

@app.route('/order_return/<order_id>')
@login_required
def order_return(order_id):
    _id = ObjectId(order_id)
    order = Order.get_order({"_id": _id})
    if order:
        if current_user.admin or current_user.id == order.user_id:
            order.return_order()
            flash("Order was returned.", "info")
        else:
            flash("No permission!", "info")
    else:
        flash('Order does not exist!', 'info')
    return redirect(url_for('orders'))



###############################################
#   Books
###############################################

@app.route('/', methods=['GET', 'POST'])
@login_required
def home():
    form = BookSearchForm()
    filter = ({}, "name")
    if form.validate_on_submit():
            filter = create_query(form)
    books = Book.get_books(filter)
    if not books:
        flash("No book was found.", "info")
    return render_template('home.html', form=form, books=books)

@app.route('/book_add', methods=['GET', 'POST'])
@login_required
def book_add():
    if current_user.admin:
        form = BookForm()
        if form.validate_on_submit():
            Book(form)
            flash('book was added!', 'info')
            return redirect(url_for('home'))
        return render_template('register.html', form=form, text="Add Book")
    else:
        flash('No permission!', 'info')
        return redirect(url_for('home'))

@app.route('/book/<book_id>')
@login_required
def book(book_id):
    _id = ObjectId(book_id)
    book = Book.get_book({"_id": _id})
    if book:
        return render_template('book.html', book=book)
    else:
        flash('Book does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/book_edit/<book_id>', methods=['GET', 'POST'])
@login_required
def book_edit(book_id):
    _id = ObjectId(book_id) 
    book = Book.get_book({"_id": _id})
    if book:
        if current_user.admin:           
            form = BookForm(obj=book)
            if form.validate_on_submit():
                fields = create_fields(form)
                fields["approved"] = False
                book.update_book(fields)
                flash('Book was edited!', 'info')
                return redirect(url_for('book', book_id=book_id))    
            return render_template('register.html', form=form, text="Edit book")    
        else:
            flash('No permission!', 'info')
            return redirect(url_for('book', book_id=book_id))
    else:
        flash('Book does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/book_delete/<book_id>')
@login_required
def book_delete(book_id):
    _id = ObjectId(book_id) 
    book = Book.get_book({"_id": _id})
    if book:
        orders = Order.get_orders({"book_id": _id, "active": True})
        if current_user.admin and not orders:
            book.delete_book()
            flash('Book was removed!', 'info')
        else:
            flash('No permission!', 'info')
    else:
        flash('Book does not exist!', 'info')
    return redirect(url_for('home'))


###############################################
#   User
###############################################

@app.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    if current_user.admin:
        form = UserSearchForm()
        filter = ({"approved": True}, "username")
        filter_approve = ({"approved": False}, "username")
        if form.validate_on_submit():
            filter = create_query(form)
            filter_approve = create_query(form)
            filter[0]["approved"] = True
            filter_approve[0]["approved"] = False
        need_approval = User.get_users(filter_approve)
        rest = User.get_users(filter)
        if not rest and not need_approval:
            flash("No user was found.", "info")
        return render_template('users.html', form=form, need_approval=need_approval, rest=rest)
    else:
        flash('No permission!', 'info')
        return redirect(url_for('home'))

@app.route('/user_add', methods=['GET', 'POST'])
@login_required
def user_add():
    if current_user.admin:
        form = RegisterForm()
        if form.validate_on_submit():
            user = User(form, bcrypt=bcrypt)
            flash('User was added!', 'info')
            return redirect(url_for('user', user_id=user.id))
        return render_template('register.html', form=form, text="Add user")
    else:
        flash('No permission!', 'info')
        return redirect(url_for('home'))

@app.route('/user_approve/<user_id>')
@login_required
def user_approve(user_id):
    _id = ObjectId(user_id) 
    user = User.get_user({"_id": _id})
    if user:
        if current_user.admin:
            user.update_user({"approved": True})
            flash('User was approved!', 'info')
            return redirect(url_for('users'))
        else:
            flash('No permission!', 'info')
            return redirect(url_for('user', user_id=user_id))
    else:
        flash('User does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/user_ban/<user_id>')
@login_required
def user_ban(user_id):
    _id = ObjectId(user_id) 
    user = User.get_user({"_id": _id})
    if user:
        if current_user.admin:       
            user.update_user({"ban": True})
            flash('User was banned!', 'info')
            return redirect(url_for('users'))
        else:
            flash('No permission!', 'info')
            return redirect(url_for('user', user_id=user_id))
    else:
        flash('User does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/user_unban/<user_id>')
@login_required
def user_unban(user_id):
    _id = ObjectId(user_id) 
    user = User.get_user({"_id": _id})
    if user:
        if current_user.admin:
            user.update_user({"ban": False})
            flash('User was unbanned!', 'info')
            return redirect(url_for('users'))
        else:
            flash('No permission!', 'info')
            return redirect(url_for('user', user_id=user_id))
    else:
        flash('User does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/user/<user_id>')
@login_required
def user(user_id):
    _id = ObjectId(user_id)
    user = User.get_user({"_id": _id})
    if user:
        if current_user.admin or current_user.id == user.id:
            return render_template('user.html', user=user)
        else:
            flash('No permission!', 'info')
            return redirect(url_for('home'))
    else:
        flash('User does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/user_edit/<user_id>', methods=['GET', 'POST'])
@login_required
def user_edit(user_id):
    _id = ObjectId(user_id) 
    user = User.get_user({"_id": _id})
    if user:
        if current_user.admin or current_user.id == user.id:           
            form = EditUserForm(obj=user)
            if form.validate_on_submit():
                fields = create_fields(form)
                fields["approved"] = False
                user.update_user(fields)
                flash('User was edited!', 'info')
                return redirect(url_for('user', user_id=user_id))    
            return render_template('register.html', form=form, text="Edit user")    
        else:
            flash('No permission!', 'info')
            return redirect(url_for('user', user_id=user_id))
    else:
        flash('User does not exist!', 'info')
        return redirect(url_for('home'))

@app.route('/user_delete/<user_id>')
@login_required
def user_delete(user_id):
    _id = ObjectId(user_id) 
    user = User.get_user({"_id": _id})
    if user:
        orders = Order.get_orders({"user_id": _id, "active": True})
        if current_user.admin and not user.admin and not orders:
            user.delete_user()
            flash('User was removed!', 'info')
        else:
            flash('No permission!', 'info')
        return redirect(url_for('users'))
    else:
        flash('User does not exist!', 'info')
        return redirect(url_for('home'))


###############################################
#   Loging
###############################################

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_user({"username": form.username.data})
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                return redirect(url_for('home'))
            else:
                flash('Invalid password!', 'info')
        else:
            flash('Invalid username!', 'info')
    return render_template('login.html', form=form)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        User(form, bcrypt=bcrypt)
        return redirect(url_for('login'))
    return render_template('register.html', form=form, text="Sing up")


if __name__ == "__main__":
    app.run(debug=True)