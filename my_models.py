from flask_login import UserMixin
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime


db_client = MongoClient("mongodb+srv://root:root1234@cluster0.sfw9omu.mongodb.net/?retryWrites=true&w=majority")
db = db_client.library

def create_fields(form):
    fields = {}
    for item in form:
        if item.id in ["submit", "csrf_token"]:
            continue
        else:
            if item.data:
                if item.id == "date":
                    fields[item.id] = datetime.combine(item.data, datetime.min.time())
                else:
                    fields[item.id] = item.data
    return fields

def create_query(form):
    sort_by = None
    filter = {}
    for item in form:
        if item.id == "sort_by":
            sort_by = item.data
        elif item.id in ["submit", "csrf_token"]:
            continue
        else:
            if item.data:
                if item.id == "date_from":
                    if "date" in filter:
                        lte = filter.pop("date")
                        filter["$and"] = [
                            {"date": {"$gte": datetime.combine(item.data, datetime.min.time())}},
                            {"date": lte}
                        ]
                    else:
                        filter["date"] = {"$gte": datetime.combine(item.data, datetime.min.time())}
                elif item.id == "date_to":
                    if "date" in filter:
                        gte = filter.pop("date")
                        filter["$and"] = [
                            {"date": {"$lte": datetime.combine(item.data, datetime.min.time())}},
                            {"date": gte}
                        ]
                    else:
                        filter["date"] = {"$lte": datetime.combine(item.data, datetime.min.time())}
                else:
                    filter[item.id] = {"$regex": item.data}
    return filter, sort_by

class User(UserMixin):
    def __init__(self, data, is_admin=False, is_approved=False, is_ban=False, bcrypt=None):
        super().__init__()
        if isinstance(data, dict):
            self.username = data["username"]
            self.password = data["password"]
            self.first_name = data["first_name"]
            self.last_name = data["last_name"]
            self.personal_number = data["personal_number"]
            self.address = data["address"]
            self.admin = data["admin"]
            self.approved = data["approved"]
            self.ban = data["ban"]
            self.id = data["_id"]
        else:
            self.username = data.username.data
            self.password = bcrypt.generate_password_hash(data.password.data)
            self.first_name = data.first_name.data
            self.last_name = data.last_name.data
            self.personal_number = data.personal_number.data
            self.address = data.address.data
            self.admin = is_admin
            self.approved = is_approved
            self.ban = is_ban
            self.id = db.users.insert_one(self.__dict__)

    def update_user(self, fields):
        _id = ObjectId(self.id)
        db.users.update_one({"_id": _id}, {"$set": fields})

    def delete_user(self):
        _id = ObjectId(self.id)
        db.users.delete_one({"_id": _id})

    @staticmethod
    def get_user(filter):
        user = db.users.find_one(filter)
        if user:
            user = User(user)
        return user

    @staticmethod
    def get_users(filter):
        users_dict = db.users.find(filter[0]).sort(filter[1])
        users = []
        for user in users_dict:
            users.append(User(user))
        return users


class Book():
    def __init__(self, data):
        if isinstance(data, dict):
            self.name = data["name"]
            self.author = data["author"]
            self.page_count = data["page_count"]
            self.date = data["date"]
            self.image = data["image"]
            self.copy_count = data["copy_count"]
            self.id = data["_id"]
        else:
            self.name = data.name.data
            self.author = data.author.data
            self.page_count = data.page_count.data
            self.date = datetime.combine(data.date.data, datetime.min.time())
            self.image = data.image.data
            self.copy_count = data.copy_count.data
            self.id = db.books.insert_one(self.__dict__)

    def update_book(self, fields):
        _id = ObjectId(self.id)
        db.books.update_one({"_id": _id}, {"$set": fields})

    def delete_book(self):
        _id = ObjectId(self.id)
        db.books.delete_one({"_id": _id})

    @staticmethod
    def get_book(filter):
        book = db.books.find_one(filter)
        if book:
            book = Book(book)
        return book

    @staticmethod
    def get_books(filter):
        books_dict = db.books.find(filter[0]).sort(filter[1])
        books = []
        for book in books_dict:
            books.append(Book(book))
        return books


class Order():
    def __init__(self, data):
        if isinstance(data, dict):
            self.user_id = data["user_id"]
            self.book_id = data["book_id"]
            self.start_date = data["start_date"]
            self.end_date = data["end_date"]
            self.active = data["active"]
            self.id = data["_id"]
            if "user" in data:
                self.user = data["user"]
            if "book" in data:
                self.book = data["book"]
        else:
            self.user_id = data[0]
            self.book_id = data[1]
            self.start_date = datetime.now()
            self.end_date = None
            self.active = True
            self.id = db.orders.insert_one(self.__dict__)

    def delete_order(self):
        _id = ObjectId(self.id)
        db.orders.delete_one({"_id": _id})

    def return_order(self):
        _id = ObjectId(self.id)
        db.orders.update_one({"_id": _id}, {"$set": {"end_date": datetime.now(), "active": False}})

    @staticmethod
    def get_order(filter):
        order = db.orders.find_one(filter)
        if order:
            order = Order(order)
        return order

    @staticmethod
    def get_orders(filter):
        orders_dict = db.orders.aggregate([
            {
                "$lookup": {
                    "from": "users",
                    "localField": "user_id",
                    "foreignField": "_id",
                    "as":"user"
                }
            },
            {
                "$lookup": {
                    "from": "books",
                    "localField": "book_id",
                    "foreignField": "_id",
                    "as": "book"
                }
            },
            {
                "$match": filter
            },
            {
                "$sort": {
                    "start_date": -1
                }
            }
        ])
        orders = []
        for order in orders_dict:
            orders.append(Order(order))
        return orders
