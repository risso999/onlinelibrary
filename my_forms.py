from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, SubmitField, MonthField, FileField, IntegerField
from wtforms.validators import InputRequired, Length, ValidationError, Regexp, Optional
from my_models import User

class RegisterForm(FlaskForm):
    username = StringField(validators=[InputRequired(), Length(min=4, max=20)])
    password = PasswordField(validators=[InputRequired(), Length(min=8, max=20)])
    first_name = StringField(validators=[InputRequired(), Length(max=20)])
    last_name = StringField(validators=[InputRequired(), Length(max=20)])
    personal_number = StringField(validators=[InputRequired(), Length(min=10, max=10)])
    address = StringField(validators=[InputRequired(), Length(max=500)])

    def validate_username(self, username):
        existing_user_username = User.get_user({"username": username.data})
        if existing_user_username:
            raise ValidationError('The username already exists!')

class EditUserForm(FlaskForm):
    first_name = StringField(validators=[InputRequired(), Length(max=20)])
    last_name = StringField(validators=[InputRequired(), Length(max=20)])
    personal_number = StringField(validators=[InputRequired(), Length(min=10, max=10)])
    address = StringField(validators=[InputRequired(), Length(max=500)])

class LoginForm(FlaskForm):
    username = StringField(validators=[InputRequired(), Length(min=4, max=20)])
    password = PasswordField(validators=[InputRequired(), Length(min=8, max=20)])

class UserSearchForm(FlaskForm):
    username = StringField(validators=[Length(min=3, max=20), Optional()])
    first_name = StringField(validators=[Length(min=3, max=20), Optional()])
    last_name = StringField(validators=[Length(min=3, max=20), Optional()])
    personal_number = StringField(validators=[Length(min=3, max=10), Optional()])
    address = StringField(validators=[Length(min=3, max=500), Optional()])
    sort_by = SelectField(choices=[('username', 'Username'), ('first_name', 'First name'), ('last_name', 'Last name'), ('personal_number', 'Personal number'), ('address', 'Address')])
    submit = SubmitField(label="Search", render_kw={'class': "btn btn-dark"})


class BookForm(FlaskForm):
    name = StringField(validators=[InputRequired(), Length(min=3, max=100)])
    author = StringField(validators=[InputRequired(), Length(min=3, max=100)])
    page_count = IntegerField(validators=[InputRequired()])
    date = MonthField(validators=[InputRequired()])
    image = FileField(validators=[Regexp('.+\.jpg$')])
    copy_count = IntegerField(validators=[InputRequired()])

class BookSearchForm(FlaskForm):
    name = StringField(validators=[Length(min=3, max=100), Optional()])
    author = StringField(validators=[Length(min=3, max=100), Optional()])
    date_from = MonthField(validators=[Optional()])
    date_to = MonthField(validators=[Optional()])
    sort_by = SelectField(label="Sort the books by", choices=[('name', 'Name'), ('author', 'Author'), ('date', 'Date')])
    submit = SubmitField(label="Search", render_kw={'class': "btn btn-dark"})

class OrderForm(FlaskForm):
    username = StringField(label="Is created for username", validators=[InputRequired(), Length(min=4, max=20)])
    name = StringField(label="Name of book", validators=[InputRequired(), Length(min=3, max=100)])
